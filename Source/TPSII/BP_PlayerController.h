// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BP_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TPSII_API ABP_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
};
