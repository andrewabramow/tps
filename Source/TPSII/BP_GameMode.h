// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BP_GameMode.generated.h"

/**
 * 
 */
UCLASS()
class TPSII_API ABP_GameMode : public AGameModeBase
{
	GENERATED_BODY()
	
};
